import * as React from 'react';
// view components
import { MenuItem } from '@md-ui/menu-items/main';
import { Logo } from '@md-ui/logos/main';
import { CartHeaderIcon } from '@md-modules/shared/components/ui/headers/cart';
// constants
import { menuItems } from './constants';
// views
import { Wrapper, IWrapper, LWrapper, RWrapper } from './views';

const Header = () => (
  <Wrapper>
    <IWrapper>
      <LWrapper>
        <Logo />
      </LWrapper>
      <RWrapper>
        {menuItems.map(({ label, href }) => (
          <MenuItem key={label} href={href} label={label} />
        ))}
      </RWrapper>
      <CartHeaderIcon />
    </IWrapper>
  </Wrapper>
);

export { Header };
