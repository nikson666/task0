import * as React from 'react';
// modal
import Modal from '@md-modules/shared/components/modal';
// views
import { CartWrapper, CartImgWrapper, CartImg, QuantityWrapper, Quantity } from './views';
// components
import { CartContainer } from '@md-sh/cart';
// context
import { CartContext } from '@md-sh/cart/context';
import { ModalCart } from '@md-modules/shared/components/modal/cart';

const CartHeaderIcon = () => {
  const [showModal, setShowModal] = React.useState<boolean>(false);
  const { cardIds } = React.useContext(CartContext);

  const onClose = () => {
    setShowModal(false);
  };

  React.useEffect(() => {
    if (!cardIds.length) {
      onClose();
    }
  }, [cardIds]);

  const onOpen = () => {
    if (!cardIds.length) {
      alert('Cart is empty');
    } else {
      setShowModal(true);
    }
  };

  return (
    <CartWrapper>
      <CartImgWrapper>
        {cardIds.length ? (
          <QuantityWrapper>
            <Quantity>{cardIds.length}</Quantity>
          </QuantityWrapper>
        ) : null}
        <CartImg onClick={() => onOpen()} src='/static/cart/cart.png' alt='img' />
        <Modal>
          <ModalCart title='Cart' onClose={onClose} isShow={showModal}>
            <CartContainer />
          </ModalCart>
        </Modal>
      </CartImgWrapper>
    </CartWrapper>
  );
};

export { CartHeaderIcon };
