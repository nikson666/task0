import styled from 'styled-components';

export const CartWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
`;

export const CartImgWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 5px;

  &:hover {
    animation: rotate-center 0.6s ease-in-out both;
  }

  @keyframes rotate-center {
    0% {
      -webkit-transform: rotate(0);
      transform: rotate(0);
    }
    100% {
      -webkit-transform: rotate(360deg);
      transform: rotate(360deg);
    }
  }
`;

export const CartImg = styled.img`
  display: block;
  width: 100%;
  height: 100%;
  max-width: 30px;
`;

export const QuantityWrapper = styled.div`
  position: relative;
  margin-top: -2vh;

  display: flex;
  justify-content: center;
  align-items: center;
  text-align: center;

  width: 10px;
  height: 10px;
  border-radius: 50%;
  background-color: white;
`;

export const Quantity = styled.p`
  font-size: 10px;
  padding: 0;
  margin: 0;
`;
