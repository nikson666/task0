import * as React from 'react';
// components
import Link from 'next/link';
// views
import { LogoWrapper, LogoLink } from './views';

const Logo = () => (
  <Link href='/' passHref>
    <LogoWrapper>
      <LogoLink>Shop</LogoLink>
    </LogoWrapper>
  </Link>
);

export { Logo };
