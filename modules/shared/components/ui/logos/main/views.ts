import styled from 'styled-components';

export const LogoWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const LogoLink = styled.h3`
  font-size: 20px;
  color: white;
  cursor: pointer;
  padding: 0;
  margin: 0;
`;
