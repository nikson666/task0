import * as React from 'react';
// components
import Link from 'next/link';
// libs
import styled from 'styled-components';

interface Props {
  href: string;
  label: string;
}

const MenuI = styled.div`
  padding: 5px 10px;

  transition: all 0.3s ease-in-out;

  a {
    text-decoration: none;
    transition: opacity 0.3s ease-in-out;

    color: ${({ theme }) => theme.colors.white};

    &:hover {
      opacity: 0.8;
    }
  }

  &:hover {
    border-bottom: 1px solid ${({ theme }) => theme.colors.white};
  }
`;

const MenuItem: React.FC<Props> = ({ href, label }) => (
  <MenuI>
    <Link href={href} passHref>
      <a>{label}</a>
    </Link>
  </MenuI>
);

export { MenuItem };
