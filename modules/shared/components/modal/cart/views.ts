import styled from 'styled-components';

export const StyledModalBody = styled.div`
  padding-top: 10px;
`;

export const StyledModalTitle = styled.h2`
  margin: 0;
  padding: 0;
  display: flex;
  justify-content: center;
`;

export const StyledModalHeaderCloseBtn = styled.button`
  padding: 10px;
  font-size: 18px;
  background-color: transparent;
  border-radius: 10px;
  border: none;
  color: ${({ theme }) => theme.colors.gray500};
  outline: none;
  transition: background-color 0.2s linear;
  cursor: pointer;

  &:hover {
    background-color: ${({ theme }) => theme.colors.gray500};
    color: ${({ theme }) => theme.colors.white};
  }
`;

export const StyledModalHeader = styled.div`
  position: fixed;
  top: 15vh;
  left: 15vw;
  width: 70vw;
  border-top-right-radius: 10px;
  border-top-left-radius: 10px;
  border-bottom: 2px solid ${({ theme }) => theme.colors.gray500};
  padding: 10px 15px;
  z-index: 100;

  display: flex;
  justify-content: space-between;
  align-items: center;
  font-size: 25px;
  background-color: ${({ theme }) => theme.colors.white};
`;

export const StyledModal = styled.div`
  background: white;
  height: 70vh;
  width: 70vw;
  border-radius: 15px;
  padding: 15px;
  margin: 20px;

  overflow: scroll;
  overflow-x: hidden;
`;
export const StyledModalOverlay = styled.div`
  position: fixed;
  z-index: 10000;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: rgba(0, 0, 0, 0.5);
`;

export const StyleModalFooter = styled.div`
  position: fixed;
  bottom: 15vh;
  left: 15vw;
  width: 70vw;
  border-bottom-right-radius: 10px;
  border-bottom-left-radius: 10px;
  border-top: 2px solid ${({ theme }) => theme.colors.gray500};
  padding: 10px 15px 25px;
  z-index: 100;

  display: flex;
  justify-content: space-around;
  align-items: center;
  font-size: 25px;
  background-color: ${({ theme }) => theme.colors.white};
`;

export const StyleModalContinueShoppingBtn = styled.button`
  padding: 10px;
  margin-right: 10px;
  font-size: 18px;
  background-color: transparent;
  border-radius: 10px;
  border: 1px solid ${({ theme }) => theme.colors.gray500};
  color: ${({ theme }) => theme.colors.gray500};
  outline: none;
  transition: background-color 0.2s linear;
  cursor: pointer;

  &:hover {
    background-color: ${({ theme }) => theme.colors.gray500};
    color: ${({ theme }) => theme.colors.white};
  }
`;

export const TotalPriceWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  text-align: center;
`;

export const TotalPrice = styled.button`
  padding: 10px;
  font-size: 25px;
  background-color: lightgreen;
  border-radius: 10px;
  border: 1px solid ${({ theme }) => theme.colors.gray500};
  color: ${({ theme }) => theme.colors.gray500};
  outline: none;
  transition: background-color 0.2s linear;
  cursor: pointer;

  &:hover {
    background-color: ${({ theme }) => theme.colors.gray500};
    color: ${({ theme }) => theme.colors.white};
  }
`;
