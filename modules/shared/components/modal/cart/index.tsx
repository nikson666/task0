import * as React from 'react';
// views
import {
  StyledModal,
  StyledModalBody,
  StyledModalHeader,
  StyledModalHeaderCloseBtn,
  StyledModalOverlay,
  StyledModalTitle,
  StyleModalContinueShoppingBtn,
  StyleModalFooter,
  TotalPrice,
  TotalPriceWrapper
} from '@md-modules/shared/components/modal/cart/views';
import Link from 'next/link';
// context
import { CartContext } from '@md-sh/cart/context';

interface IProps {
  isShow?: boolean;

  onClose?(): void | boolean;

  children?: React.ReactNode;
  title?: string;
}

const ModalCart: React.FC<IProps> = ({ children, title, onClose, isShow }) => {
  const { totalPrice } = React.useContext(CartContext);

  const handleCloseClick = (e: React.MouseEvent<HTMLElement>): void => {
    e.preventDefault();
    onClose?.();
  };

  // if (!cardIds.length) {
  //   onClose?.();
  // }

  return isShow ? (
    <StyledModalOverlay onClick={handleCloseClick}>
      <StyledModal onClick={(event) => event.stopPropagation()}>
        <StyledModalHeader>
          {title && <StyledModalTitle>{title}</StyledModalTitle>}
          <StyledModalHeaderCloseBtn onClick={handleCloseClick}>&times;</StyledModalHeaderCloseBtn>
        </StyledModalHeader>
        <StyledModalBody>{children}</StyledModalBody>
        <StyleModalFooter>
          <StyleModalContinueShoppingBtn onClick={handleCloseClick}>Continue shopping</StyleModalContinueShoppingBtn>
          <TotalPriceWrapper>
            <Link href='/'>
              <TotalPrice>Checkout {totalPrice}$</TotalPrice>
            </Link>
          </TotalPriceWrapper>
        </StyleModalFooter>
      </StyledModal>
    </StyledModalOverlay>
  ) : null;
};

export { ModalCart };
