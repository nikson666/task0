import * as React from 'react';
import * as ReactDOM from 'react-dom';

const Modal: React.FC = ({ children }) => {
  const [isBrowser, setIsBrowser] = React.useState<boolean>(false);

  React.useEffect(() => {
    setIsBrowser(true);
  }, []);

  if (isBrowser) {
    return ReactDOM.createPortal(children, document.getElementById('modal-root') as HTMLElement);
  } else {
    return null;
  }
};

export default Modal;
