import * as React from 'react';

// Entity = obj
const getEntityMock = <A>(data: A): Promise<A> => new Promise((resolve) => setTimeout(() => resolve(data), 500));

interface QueryState<A> {
  data: A | undefined;
  loading: boolean;
}

export const useQuery = <A>(resolve: A): QueryState<A> => {
  const [{ data, loading }, setState] = React.useState<QueryState<A>>({
    data: undefined,
    loading: true
  });

  React.useEffect(() => {
    getEntityMock(resolve)
      .then((data) => setState((s) => ({ ...s, data })))
      .finally(() => setState((s) => ({ ...s, loading: false })));
  }, [resolve]);

  return {
    data,
    loading
  };
};
