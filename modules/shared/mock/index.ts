export interface Technic {
  name: string;
  price: number;
  description: string;
  id: string;
  img: string;
}

export const products: Technic[] = [
  {
    name: 'Samsung',
    price: 450,
    description:
      'Удобная панель — более удобное управление. Легко читаемый дисплей с поворотным регулятором Jog Dial, LED-подсветкой и сенсорными кнопками позволяет установить нужную программу и без проблем следить за процессом стирки.',
    id: '1',
    img: 'https://i.eldorado.ua//goods_images/1039034/6364699-1563780789.jpg'
  },
  {
    name: 'LG',
    price: 199,
    description:
      'Удобная панель — более удобное управление. Легко читаемый дисплей с поворотным регулятором Jog Dial, LED-подсветкой и сенсорными кнопками позволяет установить нужную программу и без проблем следить за процессом стирки.',
    id: '2',
    img: 'https://cdn.27.ua/499/4e/05/1199621_2.jpeg'
  },
  {
    name: 'Mielo',
    price: 299,
    description:
      'Удобная панель — более удобное управление. Легко читаемый дисплей с поворотным регулятором Jog Dial, LED-подсветкой и сенсорными кнопками позволяет установить нужную программу и без проблем следить за процессом стирки.',
    id: '3',
    img:
      'https://cdn.comfy.ua/media/catalog/product/cache/5/image/1125x700/62defc7f46f3fbfc8afcd112227d1181/m/i/miele_1098-min.jpg'
  },
  {
    name: 'LG',
    price: 300,
    description:
      'Удобная панель — более удобное управление. Легко читаемый дисплей с поворотным регулятором Jog Dial, LED-подсветкой и сенсорными кнопками позволяет установить нужную программу и без проблем следить за процессом стирки.',
    id: '4',
    img: 'https://100stiralok.ru/img/lg/lgf2j6ws1l/lg-f-2j6ws1l.jpg'
  },
  {
    name: 'Samsung',
    price: 150,
    description:
      'Удобная панель — более удобное управление. Легко читаемый дисплей с поворотным регулятором Jog Dial, LED-подсветкой и сенсорными кнопками позволяет установить нужную программу и без проблем следить за процессом стирки.',
    id: '5',
    img: 'https://i1.foxtrot.com.ua/product/MediumImages/6519075_0.jpg'
  },
  {
    name: 'Toshiba',
    price: 450,
    description:
      'Гарантія\n' +
      '\n' +
      '12 міс.\n' +
      'Тип машини\n' +
      '\n' +
      'автоматична ; прально-сушильна машина\n' +
      'Максимальне завантаження циклу прання-сушка\n' +
      '\n' +
      '8 кг\n' +
      'Максимальне завантаження\n' +
      '\n' +
      '8 кг\n' +
      'Кількість обертів\n' +
      '\n' +
      '1400 об./хв.\n' +
      'Програми прання\n' +
      '\n' +
      'антиалергенне ; постільна білизна ; дитяча білизна ; змішані тканини ; пухові вироби ; бавовна ; спортивний одяг ; сорочки ; делікатне/ручне ; синтетика ; вовна ; швидке\n' +
      'Клас енергозбереження\n' +
      '\n' +
      'A+++\n' +
      'Клас прання\n' +
      '\n' +
      'A',
    id: '6',
    img: 'https://i1.foxtrot.com.ua/product/MediumImages/6560397_0.jpg'
  }
];
