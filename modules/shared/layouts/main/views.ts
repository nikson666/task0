import styled from 'styled-components';

export const Wrapper = styled.div`
  background: linear-gradient(
    50deg,
    rgba(237, 237, 237, 1) 0%,
    rgba(231, 226, 255, 1) 57%,
    rgba(230, 250, 255, 1) 100%
  );
  min-height: 100%;
  padding-top: 60px;
`;
