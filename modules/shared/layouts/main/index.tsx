import * as React from 'react';
// view components
import { Header } from '@md-ui/headers/main';
// views
import { Wrapper } from './views';
// context
import { CartContextProvider } from '@md-sh/cart/context';

const MainLayout: React.FC = ({ children }) => (
  <Wrapper>
    <CartContextProvider>
      <Header />
      {children}
    </CartContextProvider>
  </Wrapper>
);

export { MainLayout };
