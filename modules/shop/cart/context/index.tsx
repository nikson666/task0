import * as React from 'react';
import { products } from '@md-modules/shared/mock';

interface IContext {
  cardIds: string[];

  addCardId(id: string): void;

  removeTechnicFromCart?(id: string): void;

  quantityPlus?(id: string): void;

  quantityMinus?(id: string): void;

  calcTotalPrice?(): void;

  totalPrice: number;
}

const CartContext = React.createContext<IContext>({
  cardIds: [],
  addCardId: () => null,
  removeTechnicFromCart: () => null,
  quantityMinus: () => null,
  quantityPlus: () => null,
  calcTotalPrice: () => null,
  totalPrice: 0
});

const CartContextProvider: React.FC = ({ children }) => {
  const [cardIds, setCardIds] = React.useState<string[]>([]);
  const [totalPrice, setTotalPrice] = React.useState(0);

  React.useEffect(() => {
    try {
      if (!localStorage.getItem('cart')) localStorage.setItem('cart', JSON.stringify([]));

      const getCartLocalStorage = localStorage.getItem('cart');
      const cardIdsFromLocalStorage = JSON.parse(getCartLocalStorage ?? '');

      setCardIds(cardIdsFromLocalStorage);
    } catch (e) {
      console.error(e.message);
    }
  }, []);

  React.useEffect(() => {
    try {
      localStorage.setItem('cart', JSON.stringify(cardIds ?? []));
    } catch (e) {
      console.error(e.message);
    }
  }, [cardIds]);

  const addCardId = (id: string) => {
    setCardIds((prev) => [...prev, id]);
  };

  const removeTechnicFromCart = (id: string) => {
    setCardIds((prev) => prev.filter((cardId) => id !== cardId));
  };

  const quantityPlus = (id: string) => {
    setCardIds((prev) => [...prev, id]);
  };

  const quantityMinus = (id: string) => {
    const index = cardIds.indexOf(id);
    if (index === -1) return cardIds;
    setCardIds((prev) => {
      prev.splice(index, 1);
      return [...prev];
    });
  };

  const calcTotalPrice = () => {
    try {
      let sumOfPrice = 0;
      cardIds.forEach((id: string) => {
        products.forEach((technic) => {
          if (technic.id === id) {
            sumOfPrice += technic.price;
          }
        });
      });
      setTotalPrice(sumOfPrice);
    } catch (e) {
      console.error(e.message);
    }
  };

  return (
    <CartContext.Provider
      value={{
        cardIds,
        addCardId,
        removeTechnicFromCart,
        quantityMinus,
        quantityPlus,
        calcTotalPrice,
        totalPrice
      }}
    >
      {children}
    </CartContext.Provider>
  );
};

export { CartContextProvider, CartContext };
