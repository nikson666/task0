import * as React from 'react';
import { CartAPIContextProvider } from '@md-sh/cart/layers/api/cart';
import { CartBLContextProvider } from '@md-sh/cart/layers/business';
import { CartPresentation } from '@md-sh/cart/layers/presentation';

const CartContainer = () => (
  <>
    <CartAPIContextProvider>
      <CartBLContextProvider>
        <CartPresentation />
      </CartBLContextProvider>
    </CartAPIContextProvider>
  </>
);

export { CartContainer };
