import * as React from 'react';
// views
import {
  CardWrapper,
  CardHeader,
  CardHeaderTitle,
  CardImgWrapper,
  CardImg,
  CardFooter,
  DetailsButton,
  PriceWrapper,
  Price,
  RemoveButton,
  CardContentWrapper,
  CardDescription,
  QuantityOfItemWrapper,
  Minus,
  Plus,
  QuantityOfItem,
  QuantityPrice
} from './views';
// views components
import { TechnicLink } from '@md-sh/products/components/technic-link';
import { CartContext } from '@md-sh/cart/context';

interface Props {
  id: string;
  name: string;
  price: number;
  img: string;
  description: string;
  quantity: string[];
}

const Card: React.FC<Props> = ({ id, name, price, img, description, quantity }) => {
  const { removeTechnicFromCart, quantityMinus, quantityPlus } = React.useContext(CartContext);
  return (
    <CardWrapper key={id}>
      <RemoveButton onClick={() => removeTechnicFromCart?.(id)}>&times;</RemoveButton>
      <CardHeader>
        <TechnicLink pId={id}>
          <CardHeaderTitle>{name}</CardHeaderTitle>
        </TechnicLink>
      </CardHeader>
      <CardContentWrapper>
        <TechnicLink pId={id}>
          <CardImgWrapper>
            <CardImg src={img} alt={'IMG'} />
          </CardImgWrapper>
        </TechnicLink>
        <CardDescription>{description}</CardDescription>
        <CardFooter>
          <TechnicLink pId={id}>
            <DetailsButton>Details</DetailsButton>
          </TechnicLink>
          <PriceWrapper>
            Price:
            <Price>{price * quantity.length}</Price>$
          </PriceWrapper>
          <QuantityOfItemWrapper>
            <Minus onClick={() => quantityMinus?.(id)}>-</Minus>
            <QuantityOfItem>
              <QuantityPrice>{quantity.length}</QuantityPrice>
            </QuantityOfItem>
            <Plus onClick={() => quantityPlus?.(id)}>+</Plus>
          </QuantityOfItemWrapper>
        </CardFooter>
      </CardContentWrapper>
    </CardWrapper>
  );
};
export { Card };
