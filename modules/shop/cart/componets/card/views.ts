import styled from 'styled-components';

export const CardWrapper = styled.div`
  position: relative;
  border-radius: 15px;
  border: 1px solid ${({ theme }) => theme.colors.gray600};
  overflow: hidden;
  margin: 1rem;
`;

export const CardHeader = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 10px;
  background-color: ${({ theme }) => theme.colors.white};
`;

export const CardHeaderTitle = styled.h2`
  font-weight: bold;
  line-height: 1.28;
  color: ${({ theme }) => theme.colors.gray500};
  margin: 0;
  text-decoration: none;
`;

export const CardContentWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: ${({ theme }) => theme.colors.white};

  @media (max-width: 800px) {
    display: flex;
    flex-direction: column;
  }
`;

export const CardImgWrapper = styled.div`
  margin: 0 20px 20px;
  position: relative;
`;

export const CardImg = styled.img`
  position: relative;
  top: 0;
  left: 0;
  width: 200px;
  height: 100%;
  display: block;
`;

export const CardDescription = styled.p`
  font-size: 16px;
  font-weight: bold;
  padding: 0;
  margin: 0 0 0 5px;

  @media (max-width: 1000px) {
    display: none;
  }
`;

export const CardFooter = styled.div`
  display: flex;
  flex-direction: column-reverse;
  justify-content: space-between;
  align-items: center;

  background-color: ${({ theme }) => theme.colors.white};
  padding: 10px 25px 15px;

  @media (max-width: 800px) {
    display: flex;
    flex-direction: column;
    min-width: 100%;
  }
`;

export const DetailsButton = styled.button`
  padding: 10px;
  font-size: xx-large;
  background-color: transparent;
  border-radius: 10px;
  border: 1px solid ${({ theme }) => theme.colors.gray500};
  color: ${({ theme }) => theme.colors.gray500};
  outline: none;
  transition: background-color 0.2s linear;
  cursor: pointer;

  &:hover {
    background-color: ${({ theme }) => theme.colors.gray500};
    color: ${({ theme }) => theme.colors.white};
  }
`;

export const PriceWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: xx-large;
  padding: 10px;
`;

export const Price = styled.p`
  color: ${({ theme }) => theme.colors.red};
  font-size: xx-large;
  font-weight: bold;
  padding: 0;
  margin: 0 0 0 5px;
`;

export const RemoveButton = styled.button`
  position: absolute;
  top: 5px;
  right: 5px;

  padding: 5px 10px;
  background-color: transparent;

  border-radius: 6px;
  border: none;
  color: ${({ theme }) => theme.colors.gray500};
  outline: none;
  transition: background-color 0.2s linear;
  cursor: pointer;

  &:hover {
    background-color: ${({ theme }) => theme.colors.gray500};
    color: ${({ theme }) => theme.colors.white};
  }
`;

export const QuantityOfItemWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const QuantityOfItem = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  padding: 10px;
`;

export const QuantityPrice = styled.p`
  margin: 0;
  padding: 0;
  font-size: 25px;
  font-weight: normal;
`;

export const Minus = styled.button`
  position: relative;
  background-color: transparent;

  border-radius: 6px;
  border: none;
  color: ${({ theme }) => theme.colors.gray500};
  outline: none;
  transition: background-color 0.2s linear;
  font-size: 18px;
  cursor: pointer;

  &:hover {
    background-color: ${({ theme }) => theme.colors.gray500};
    color: ${({ theme }) => theme.colors.white};
  }
`;

export const Plus = styled.button`
  position: relative;
  background-color: transparent;

  border-radius: 6px;
  border: none;
  color: ${({ theme }) => theme.colors.gray500};
  outline: none;
  transition: background-color 0.2s linear;
  font-size: 18px;
  cursor: pointer;

  &:hover {
    background-color: ${({ theme }) => theme.colors.gray500};
    color: ${({ theme }) => theme.colors.white};
  }
`;
