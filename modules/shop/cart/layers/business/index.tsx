import * as React from 'react';
// context
import { CartAPIContext } from '@md-sh/cart/layers/api/cart';
import { CartContext } from '@md-sh/cart/context';
// mock
import { Technic } from '@md-modules/shared/mock';

interface Context {
  cartList: Pick<Technic, 'name' | 'id' | 'price' | 'description' | 'img'>[];
  cardIds: string[];
}

const CartBLContext = React.createContext<Context>({
  cartList: [],
  cardIds: []
});

const CartBLContextProvider: React.FC = ({ children }) => {
  // add BL here
  const { products } = React.useContext(CartAPIContext);
  const { cardIds, calcTotalPrice } = React.useContext(CartContext);

  React.useEffect(() => {
    try {
      localStorage.setItem('cart', JSON.stringify(cardIds));
      calcTotalPrice?.();
    } catch (e) {
      console.error(e.message);
    }
  }, [cardIds, calcTotalPrice]);

  const cartList = React.useMemo<Pick<Technic, 'name' | 'id' | 'price' | 'description' | 'img'>[]>(() => {
    return products?.filter(({ id }) => cardIds?.includes(id)) ?? [];
  }, [cardIds, products]);

  return (
    <CartBLContext.Provider
      value={{
        cartList,
        cardIds
      }}
    >
      {children}
    </CartBLContext.Provider>
  );
};

export { CartBLContextProvider, CartBLContext };
