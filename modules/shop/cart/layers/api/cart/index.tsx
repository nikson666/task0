import * as React from 'react';
// utils
import { useQuery } from '@md-utils/mock/query';
// mock
import { Technic, products } from '@md-modules/shared/mock';

interface Context {
  products: Technic[] | undefined;
  isLoading: boolean;
}

const CartAPIContext = React.createContext<Context>({
  products: [],
  isLoading: false
});

const CartAPIContextProvider: React.FC = ({ children }) => {
  // add api call here
  const { data, loading } = useQuery(products);

  return (
    <CartAPIContext.Provider
      value={{
        products: data,
        isLoading: loading
      }}
    >
      {children}
    </CartAPIContext.Provider>
  );
};

export { CartAPIContextProvider, CartAPIContext };
