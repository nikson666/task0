import * as React from 'react';
// view component
import { ContentLoader } from '@md-ui/loaders/content-loader';
import { Card } from '@md-sh/cart/componets/card';
// context
import { CartAPIContext } from '@md-sh/cart/layers/api/cart';
import { CartBLContext } from '@md-sh/cart/layers/business';
// view
import { Wrapper } from './views';

const CartPresentation = () => {
  const { isLoading } = React.useContext(CartAPIContext);
  const { cartList, cardIds } = React.useContext(CartBLContext);

  return (
    <Wrapper>
      <ContentLoader isLoading={isLoading}>
        {cartList.map((technic) => (
          <Card
            quantity={cardIds?.filter((id) => technic?.id === id)}
            {...technic}
            key={technic?.id + (Math.random() * 100).toString()}
          />
        ))}
      </ContentLoader>
    </Wrapper>
  );
};

export { CartPresentation };
