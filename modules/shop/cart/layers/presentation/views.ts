import styled from 'styled-components';

export const Wrapper = styled.div`
  padding: 30px 0 60px;
  margin: 0 auto;
  display: grid;
  grid-gap: 5px;
  grid-template-columns: repeat(auto-fill, minmax(100%, 1fr));

  @media (max-width: 695px) {
    padding: 30px 0 90px;
  }
`;
