import * as React from 'react';
// context
import { TechnicAPIContext } from '@md-sh-technic/layers/api/technic';
import { TechnicBLContext } from '@md-sh-technic/layers/business';
import { CartContext } from '@md-sh/cart/context';
// views components
import { ContentLoader } from '@md-ui/loaders/content-loader';
import { TechnicInfo } from '@md-sh/technic/components/technic-info';
// views
import {
  TechnicWrapper,
  TechnicContentWrapper,
  TechnicDetailsWrapper,
  TechnicIMG,
  TechnicIMGWrapper,
  TechnicDetailsTitleWrapper,
  TechnicDetailsTitle,
  TechnicDetails,
  TechnicAddBtn
} from './views';

const TechnicPresentation = () => {
  const { isLoading } = React.useContext(TechnicAPIContext);
  const { technicInfo, technicImg, technicId } = React.useContext(TechnicBLContext);
  const { addCardId } = React.useContext(CartContext);

  return (
    <TechnicWrapper>
      <TechnicContentWrapper>
        <ContentLoader isLoading={isLoading}>
          <TechnicIMGWrapper>
            <TechnicIMG src={technicImg} />
          </TechnicIMGWrapper>
          <TechnicDetailsTitleWrapper>
            <TechnicDetailsTitle>Details:</TechnicDetailsTitle>
          </TechnicDetailsTitleWrapper>
          <TechnicDetailsWrapper>
            <TechnicDetails>
              {technicInfo.map((info, i) => (
                <TechnicInfo key={i} {...info} />
              ))}
            </TechnicDetails>
            <TechnicAddBtn onClick={() => addCardId(technicId)}>Add to cart</TechnicAddBtn>
          </TechnicDetailsWrapper>
        </ContentLoader>
      </TechnicContentWrapper>
    </TechnicWrapper>
  );
};

export { TechnicPresentation };
