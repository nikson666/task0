import styled from 'styled-components';

export const TechnicWrapper = styled.div`
  display: flex;
  align-items: center;

  padding: 50px 0;
`;

export const TechnicContentWrapper = styled.div`
  max-width: ${({ theme }) => theme.dimensions.pageMaxWidth}px;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
`;

export const TechnicIMGWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  padding-bottom: 25px;
  border-bottom: 3px solid ${({ theme }) => theme.colors.gray500};
`;

export const TechnicIMG = styled.img`
  display: block;

  width: 100%;
  height: 100%;
  max-width: 300px;
  border-radius: 5px;
`;

export const TechnicDetailsTitleWrapper = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;

  background-color: ${({ theme }) => theme.colors.gray500};
  padding: 10px 10px 5px 25px;
`;

export const TechnicDetailsTitle = styled.h3`
  color: ${({ theme }) => theme.colors.white};
  font-size: 20px;
`;

export const TechnicDetailsWrapper = styled.div`
  padding: 20px;
  border-bottom: 2px solid ${({ theme }) => theme.colors.gray500};
  background-color: ${({ theme }) => theme.colors.gray600};
`;

export const TechnicDetails = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0;
  padding: 0;
`;

export const TechnicAddBtn = styled.button`
  padding: 5px 10px;
  background-color: transparent;
  border-radius: 6px;
  border: 2px solid ${({ theme }) => theme.colors.gray500};
  color: ${({ theme }) => theme.colors.white};
  outline: none;
  transition: background-color 0.2s linear;
  cursor: pointer;
  transition: all 0.5s;

  &:hover {
    transform: scale(1.1);
    background-color: ${({ theme }) => theme.colors.gray500};
    color: ${({ theme }) => theme.colors.white};
  }
`;
