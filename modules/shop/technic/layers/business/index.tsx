import * as React from 'react';
import { TechnicAPIContext } from '@md-sh-technic/layers/api/technic';

interface TechnicInfo {
  label: string;
  value: string | number;
}

interface Context {
  technicInfo: TechnicInfo[];
  technicImg: string;
  technicId: string;
}

const TechnicBLContext = React.createContext<Context>({
  technicInfo: [],
  technicImg: '',
  technicId: ''
});

const TechnicBLContextProvider: React.FC = ({ children }) => {
  // add BL here
  const { technic } = React.useContext(TechnicAPIContext);

  const technicImg = technic?.img ?? '';

  const technicId = technic?.id ?? '';

  const technicInfo = React.useMemo<TechnicInfo[]>(
    () => {
      if (!technic) {
        return [];
      }

      return [
        {
          label: 'Name',
          value: technic.name
        },
        {
          label: 'Description',
          value: technic.description
        },
        {
          label: 'Price',
          value: technic.price
        }
      ];
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [technic]
  );

  return (
    <TechnicBLContext.Provider
      value={{
        technicInfo,
        technicImg,
        technicId
      }}
    >
      {children}
    </TechnicBLContext.Provider>
  );
};

export { TechnicBLContextProvider, TechnicBLContext };
