import * as React from 'react';
// libs
import { useRouter } from 'next/router';
// utils
import { useQuery } from '@md-utils/mock/query';
// mock
import { products, Technic } from '@md-modules/shared/mock';

interface Context {
  technic: Technic | undefined;
  isLoading: boolean;
}

const TechnicAPIContext = React.createContext<Context>({
  technic: undefined,
  isLoading: false
});

const TechnicAPIContextProvider: React.FC = ({ children }) => {
  const router = useRouter();
  // eslint-disable-next-line no-console
  console.log(router);

  const { data, loading } = useQuery(products.find(({ id }) => id === router.query.id));

  return (
    <TechnicAPIContext.Provider
      value={{
        technic: data,
        isLoading: loading
      }}
    >
      {children}
    </TechnicAPIContext.Provider>
  );
};

export { TechnicAPIContextProvider, TechnicAPIContext };
