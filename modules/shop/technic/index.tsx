import * as React from 'react';
// API
import { TechnicAPIContextProvider } from '@md-sh/technic/layers/api/technic';
// BL
import { TechnicBLContextProvider } from '@md-sh/technic/layers/business';
// Presentation
import { TechnicPresentation } from '@md-sh/technic/layers/presentation';
// Views
import { TitleWrapper, Title } from './views';
import Head from 'next/head';

const TechnicContainer = () => (
  <>
    <Head>
      <title>Technic</title>
      <link rel='icon' href='/favicon.ico' />
      <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1' />
      <meta charSet='utf-8' />
    </Head>
    <TitleWrapper>
      <Title>Technic</Title>
    </TitleWrapper>
    <TechnicAPIContextProvider>
      <TechnicBLContextProvider>
        <TechnicPresentation />
      </TechnicBLContextProvider>
    </TechnicAPIContextProvider>
  </>
);

export { TechnicContainer };
