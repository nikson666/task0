import styled from 'styled-components';

export const TitleWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 10px;
  margin-bottom: 10px;
`;

export const Title = styled.h1`
  padding: 0;
  margin: 0;
  font-weight: bold;
  color: ${({ theme }) => theme.colors.gray500};
`;
