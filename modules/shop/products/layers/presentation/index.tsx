import * as React from 'react';
// view components
import { ContentLoader } from '@md-ui/loaders/content-loader';
import { Card } from '@md-sh-products/components/card';
// context
import { ProductsAPIContext } from '@md-sh-products/layers/api/products';
import { ProductsBLContext } from '@md-sh-products/layers/business';
// views
import { Wrapper } from './views';

const ProductsPresentation = () => {
  const { isLoading } = React.useContext(ProductsAPIContext);
  const { productsList } = React.useContext(ProductsBLContext);

  return (
    <Wrapper>
      <ContentLoader isLoading={isLoading}>
        {productsList.map((technic) => (
          // @ts-ignore
          <Card {...technic} key={technic.id} />
        ))}
      </ContentLoader>
    </Wrapper>
  );
};

export { ProductsPresentation };
