import * as React from 'react';
// context
import { ProductsAPIContext } from '@md-sh-products/layers/api/products';
// mock
import { Technic } from '@md-modules/shared/mock';

interface Context {
  productsList: Pick<Technic, 'id' | 'name'>[];
}

const ProductsBLContext = React.createContext<Context>({
  productsList: []
});

const ProductsBLContextProvider: React.FC = ({ children }) => {
  // add business logic here
  const { products } = React.useContext(ProductsAPIContext);

  const productsList = React.useMemo<Pick<Technic, 'id' | 'name'>[]>(
    () => {
      return (
        products?.map(({ id, name, price, img }) => ({
          name,
          id,
          price,
          img
        })) ?? []
      );
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [typeof products === 'undefined']
  );

  return (
    <ProductsBLContext.Provider
      value={{
        productsList
      }}
    >
      {children}
    </ProductsBLContext.Provider>
  );
};

export { ProductsBLContextProvider, ProductsBLContext };
