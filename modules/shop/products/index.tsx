import * as React from 'react';
// API
import { ProductsAPIContextProvider } from '@md-sh-products/layers/api/products';
// Business
import { ProductsBLContextProvider } from '@md-sh-products/layers/business';
// Presentation component
import { ProductsPresentation } from '@md-sh-products/layers/presentation';
import Head from 'next/head';
// views
import { Title, TitleWrapper } from '@md-sh/products/views';

const ProductContainer = () => (
  <>
    <Head>
      <title>Products</title>
    </Head>
    <TitleWrapper>
      <Title>Products</Title>
    </TitleWrapper>
    <ProductsAPIContextProvider>
      <ProductsBLContextProvider>
        <ProductsPresentation />
      </ProductsBLContextProvider>
    </ProductsAPIContextProvider>
  </>
);

export { ProductContainer };
