import styled from 'styled-components';

export const AddButton = styled.button`
  padding: 5px 10px;
  background-color: transparent;
  border-radius: 6px;
  border: 1px solid ${({ theme }) => theme.colors.gray500};
  color: ${({ theme }) => theme.colors.gray500};
  outline: none;
  transition: background-color 0.2s linear;
  cursor: pointer;

  &:hover {
    background-color: ${({ theme }) => theme.colors.gray500};
    color: ${({ theme }) => theme.colors.white};
  }
`;
