import * as React from 'react';
// views
import { AddButton } from './views';
import { CartContext } from '@md-sh/cart/context';

interface Props {
  id: string;
}

const AddToCartBtn: React.FC<Props> = ({ id }) => {
  const { addCardId } = React.useContext(CartContext);

  return <AddButton onClick={() => addCardId(id)}>Add to cart</AddButton>;
};

export { AddToCartBtn };
