import * as React from 'react';
// components
import Link from 'next/link';

interface Props {
  pId: string;
}

const TechnicLink: React.FC<Props> = ({ pId, children }) => {
  return (
    <Link href='/products/[id]' as={`/products/${pId}`}>
      <a>{children}</a>
    </Link>
  );
};

export { TechnicLink };
