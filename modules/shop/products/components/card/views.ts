import styled from 'styled-components';

export const CardWrapper = styled.div`
  position: relative;
  border-radius: 15px;
  border: 1px solid ${({ theme }) => theme.colors.gray600};
  overflow: hidden;
  margin: 1rem;
`;

export const CardHeader = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 10px;
  background-color: ${({ theme }) => theme.colors.white};
`;

export const CardHeaderTitle = styled.h2`
  font-weight: bold;
  line-height: 1.28;
  color: ${({ theme }) => theme.colors.gray500};
  margin: 0;
`;

export const CardImgWrapper = styled.div`
  padding: 55% 0;
  position: relative;
`;

export const CardImg = styled.img`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  display: block;
`;

export const CardFooter = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: ${({ theme }) => theme.colors.white};
  padding: 10px 25px 15px;
`;

export const DetailsButton = styled.button`
  padding: 5px 10px;
  background-color: transparent;
  border-radius: 6px;
  border: 1px solid ${({ theme }) => theme.colors.gray500};
  color: ${({ theme }) => theme.colors.gray500};
  outline: none;
  transition: background-color 0.2s linear;
  cursor: pointer;

  &:hover {
    background-color: ${({ theme }) => theme.colors.gray500};
    color: ${({ theme }) => theme.colors.white};
  }
`;

export const PriceWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 18px;
  padding: 10px;
`;

export const Price = styled.p`
  color: ${({ theme }) => theme.colors.red};
  font-size: 18px;
  font-weight: bold;
  padding: 0;
  margin: 0 0 0 5px;
`;
