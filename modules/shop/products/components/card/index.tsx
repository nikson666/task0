import * as React from 'react';
// views
import {
  CardWrapper,
  CardHeader,
  CardHeaderTitle,
  CardImgWrapper,
  CardImg,
  CardFooter,
  DetailsButton,
  Price,
  PriceWrapper
} from './views';
// views components
import { TechnicLink } from '../technic-link';
import { AddToCartBtn } from '@md-sh/products/components/buttonAddToCart';

interface Props {
  id: string;
  name: string;
  price: number;
  img: string;
}

const Card: React.FC<Props> = ({ id, name, price, img }) => (
  <CardWrapper key={id}>
    <CardHeader>
      <TechnicLink pId={id}>
        <CardHeaderTitle>{name}</CardHeaderTitle>
      </TechnicLink>
    </CardHeader>
    <TechnicLink pId={id}>
      <CardImgWrapper>
        <CardImg src={img} alt={'IMG'} />
      </CardImgWrapper>
    </TechnicLink>
    <CardFooter>
      <TechnicLink pId={id}>
        <DetailsButton>Details</DetailsButton>
      </TechnicLink>
      <PriceWrapper>
        Price:
        <TechnicLink pId={id}>
          <Price>{price}</Price>
        </TechnicLink>
        $
      </PriceWrapper>
      <AddToCartBtn id={id} />
    </CardFooter>
  </CardWrapper>
);
export { Card };
