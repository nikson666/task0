import * as React from 'react';
import { MainLayout } from '@md-modules/shared/layouts/main';
import { ProductContainer } from '@md-sh/products';

const ProductsPage = () => (
  <MainLayout>
    <ProductContainer />
  </MainLayout>
);

export default ProductsPage;
