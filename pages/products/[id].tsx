import * as React from 'react';
import { MainLayout } from '@md-modules/shared/layouts/main';
import { TechnicContainer } from '@md-sh-technic/index';

const TechnicPage = () => {
  return (
    <MainLayout>
      <TechnicContainer />
    </MainLayout>
  );
};

export default TechnicPage;
